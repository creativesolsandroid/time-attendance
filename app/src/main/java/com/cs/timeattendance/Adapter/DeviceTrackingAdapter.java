package com.cs.timeattendance.Adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.timeattendance.Model.DeviceDetails;
import com.cs.timeattendance.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 05-10-2016.
 */
public class DeviceTrackingAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<DeviceDetails> deviceList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public DeviceTrackingAdapter(Context context, ArrayList<DeviceDetails> deviceList, String language) {
        this.context = context;
        this.deviceList = deviceList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return deviceList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView deviceName, lastSynced, lastSynced1;
//    ImageView addressType;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.device_traking_listitem, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.deviceName = (TextView) convertView
                    .findViewById(R.id.mechine_name);
//            holder.storeName = (TextView) convertView
//                    .findViewById(R.id.store_name);
            holder.lastSynced = (TextView) convertView
                    .findViewById(R.id.last_sync);
            holder.lastSynced1 = (TextView) convertView
                    .findViewById(R.id.last_sync1);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.deviceName.setText(deviceList.get(position).getDeviceName());


        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
        try {
            dateObj = curFormater1.parse(deviceList.get(position).getLastActivity());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy", Locale.US);
        String date = postFormater.format(dateObj);

        Log.e("TAG", "date" + date);


        SimpleDateFormat postFormaters = new SimpleDateFormat("hh:mm a", Locale.US);
        String dates = postFormaters.format(dateObj);

        if (DateUtils.isToday(dateObj.getTime())) {
//            date=holder.lastSynced.getText().toString().replace("at",""+R.drawable.mach_track_time);
//            Log.e("TAG","date1"+date);
            holder.lastSynced.setText(date);
            holder.lastSynced1.setText(dates);
//            holder.lastSynced.setTextColor(Color.parseColor("#2CC54E"));
        } else {
//            date=holder.lastSynced.getText().toString().replace("at",""+R.drawable.mach_track_time);
//            Log.e("TAG","date1"+date);
            holder.lastSynced.setText(date);
            holder.lastSynced1.setText(dates);
//            holder.lastSynced.setTextColor(Color.parseColor("#FF0000"));
        }

        return convertView;
    }
}