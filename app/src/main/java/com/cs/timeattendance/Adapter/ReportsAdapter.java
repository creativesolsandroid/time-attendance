package com.cs.timeattendance.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.timeattendance.R;
import com.cs.timeattendance.Model.Reports;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 13-10-2016.
 */
public class ReportsAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Reports> reportList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    int shiftType;
    //public ImageLoader imageLoader;

    public ReportsAdapter(Context context, ArrayList<Reports> reportList, String language, int shiftType) {
        this.context = context;
        this.reportList = reportList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.shiftType = shiftType;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return reportList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView date, checkIn, checkOut;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.report_details_listitem, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.report_details_listitem, null);
            }


            holder.date = (TextView) convertView
                    .findViewById(R.id.date);
            holder.checkIn = (TextView) convertView
                    .findViewById(R.id.checkin_time);
            holder.checkOut = (TextView) convertView
                    .findViewById(R.id.checkout_time);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat curFormater2 = new SimpleDateFormat("HH:mm:ss", Locale.US);
        SimpleDateFormat curFormater3 = new SimpleDateFormat("hh:mm:ss a", Locale.US);
        SimpleDateFormat curFormater4 = new SimpleDateFormat("hh:mm a", Locale.US);
        if(shiftType == 2){
            if (reportList.get(position).getClockIn().equalsIgnoreCase("null")) {
                holder.checkIn.setText("-");
            } else {
                if(reportList.get(position).getClockIn().equals("-")){
                    holder.checkIn.setText(reportList.get(position).getClockIn());
                }else{
                    Date dateObj = null;
                    try {
                        dateObj = curFormater3.parse(reportList.get(position).getClockIn());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String date = curFormater4.format(dateObj);
                    holder.checkIn.setText(date);
                }
//                holder.checkIn.setText(reportList.get(position).getClockIn());
            }

            if (reportList.get(position).getClockOut().equalsIgnoreCase("null")) {
                holder.checkOut.setText("-");
            } else {
                if(reportList.get(position).getClockOut().equals("-")){
                    holder.checkOut
                            .setText(reportList.get(position).getClockOut());
                }else{
                    Date dateObj = null;
                    try {
                        dateObj = curFormater3.parse(reportList.get(position).getClockOut());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String date = curFormater4.format(dateObj);
                    holder.checkOut.setText(date);
                }
//                holder.checkOut.setText(reportList.get(position).getClockOut());
            }
//            holder.checkIn.setText(reportList.get(position).getClockIn());
//            holder.checkOut.setText(reportList.get(position).getClockOut());
        }else {
            String[] parts1 = reportList.get(position).getCheckIn().split("T");
            String[] parts2 = reportList.get(position).getCheckOut().split("T");
            if(parts1.length > 1){
                Date dateObj = null;
                try {
                    dateObj = curFormater2.parse(parts1[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String date = curFormater4.format(dateObj);
                holder.checkIn.setText(date);
            }else {
                if(reportList.get(position).getCheckIn() != null) {
                    if (reportList.get(position).getCheckIn().equalsIgnoreCase("null")) {
                        holder.checkIn.setText("-");
                    } else {
                        holder.checkIn.setText(reportList.get(position).getCheckIn());
                    }
                }
            }

            if(parts2.length > 1){
                Date dateObj = null;
                try {
                    dateObj = curFormater2.parse(parts2[1]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String date = curFormater4.format(dateObj);
                holder.checkOut.setText(date);
            }else {
                if(reportList.get(position).getCheckOut() != null) {
                    if (reportList.get(position).getCheckOut().equalsIgnoreCase("null")) {
                        holder.checkOut.setText("-");
                    } else {
                        holder.checkOut.setText(reportList.get(position).getCheckOut());
                    }
                }
            }
        }
        Date dateObj = null;
        try {
            dateObj = curFormater1.parse(reportList.get(position).getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat postFormater = new SimpleDateFormat("dd-MM-yyyy EEE", Locale.US);
        String date = postFormater.format(dateObj);
        holder.date.setText(date);

        return convertView;
    }
}
