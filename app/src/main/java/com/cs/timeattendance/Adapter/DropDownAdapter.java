package com.cs.timeattendance.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.timeattendance.Model.Item;
import com.cs.timeattendance.Model.SectionItem;
import com.cs.timeattendance.R;

import java.util.ArrayList;

/**
 * Created by CS on 2/22/2018.
 */

public class DropDownAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Item> itemlist = new ArrayList<>();

    public DropDownAdapter(Context context, ArrayList<Item> itemlist) {
        this.context = context;
        this.itemlist = itemlist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return itemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView Name;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        final Item i = itemlist.get(position);
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.drop_down_list, null);

            holder.Name = (TextView) convertView
                    .findViewById(R.id.drop_text);
            convertView.setTag(holder);
        }
        if (i != null) {
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;
                final TextView sectionView = (TextView) convertView.findViewById(R.id.header_title);
                holder.Name.setText(si.getTitle());

                Log.i("TAG","name "+si.getTitle());

            }
        }

        return convertView;
    }
}
