package com.cs.timeattendance.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.timeattendance.Model.Employee;
import com.cs.timeattendance.R;

import java.util.ArrayList;

/**
 * Created by CS on 4/20/2017.
 */

public class EmployeeAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Employee> employeelist = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public EmployeeAdapter(Context context, ArrayList<Employee> employeelist, String language) {
        this.context = context;
        this.employeelist = employeelist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }
    @Override
    public int getCount() {
        return employeelist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView employeename, empno;
//    ImageView addressType;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
//            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.employee_list, null);
//            }else if(language.equalsIgnoreCase("Ar")){
//                convertView = inflater.inflate(R.layout.address_list_item_arabic, null);
//            }


            holder.employeename = (TextView) convertView
                    .findViewById(R.id.emp_name);
//            holder.storeName = (TextView) convertView
//   m k                  .findViewById(R.id.store_name);
            holder.empno = (TextView) convertView
                    .findViewById(R.id.emp_id);
//            holder.lastSynced1 = (TextView) convertView
//                    .findViewById(R.id.last_sync1);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.employeename.setText(employeelist.get(position).getName());

        holder.empno.setText(employeelist.get(position).getEmpno());

        return convertView;
    }
}
