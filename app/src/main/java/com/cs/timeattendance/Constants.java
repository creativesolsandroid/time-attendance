package com.cs.timeattendance;

/**
 * Created by CS on 13-10-2016.
 */
public class Constants {

    public static String TEST_URL = "http://csadms.com/AttendanceSmartPhoneServices/";

    public static String REPORTS_URL = TEST_URL + "api/AMSService/MonthlyAttendenceRpt";
    public static String MACHINE_TRACKING_URL = TEST_URL + "api/AMSService/GetDeviceStatus";
    public static String LOGIN_URL = TEST_URL + "api/AMSService/AuthenticateUser?UserName=";
    public static String EMPLOYEE_TRACKING_URL = TEST_URL +"api/AMSService/GetEmployeesInDept";
}
