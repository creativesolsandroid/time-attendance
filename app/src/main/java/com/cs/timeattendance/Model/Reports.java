package com.cs.timeattendance.Model;

/**
 * Created by CS on 13-10-2016.
 */
public class Reports {

    String userId, empNo, checkIn, checkOut, clockIn, clockOut, NClockIn, NClockOut, CInObj, COutObj, NCInObj, NCOutObj, date, day,
    durationSec, lateSec, earlySec, OTsec, OTHours, remarks, isHolidy, isLate, isEarly, isOTEligible, empShiftId, shiftId, shiftName,
    shiftText, weeklyOffs, name, workingDays, presentDays, paidDays, absentDays, totalDays, compLogo, department, designation, photo,
    lateDays, lateHalfDays, late1Day, late2Day, latePenalty, totalDurationSec, totalLateSec;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getClockOut() {
        return clockOut;
    }

    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }

    public String getNClockIn() {
        return NClockIn;
    }

    public void setNClockIn(String NClockIn) {
        this.NClockIn = NClockIn;
    }

    public String getNClockOut() {
        return NClockOut;
    }

    public void setNClockOut(String NClockOut) {
        this.NClockOut = NClockOut;
    }

    public String getCInObj() {
        return CInObj;
    }

    public void setCInObj(String CInObj) {
        this.CInObj = CInObj;
    }

    public String getCOutObj() {
        return COutObj;
    }

    public void setCOutObj(String COutObj) {
        this.COutObj = COutObj;
    }

    public String getNCInObj() {
        return NCInObj;
    }

    public void setNCInObj(String NCInObj) {
        this.NCInObj = NCInObj;
    }

    public String getNCOutObj() {
        return NCOutObj;
    }

    public void setNCOutObj(String NCOutObj) {
        this.NCOutObj = NCOutObj;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDurationSec() {
        return durationSec;
    }

    public void setDurationSec(String durationSec) {
        this.durationSec = durationSec;
    }

    public String getLateSec() {
        return lateSec;
    }

    public void setLateSec(String lateSec) {
        this.lateSec = lateSec;
    }

    public String getEarlySec() {
        return earlySec;
    }

    public void setEarlySec(String earlySec) {
        this.earlySec = earlySec;
    }

    public String getOTsec() {
        return OTsec;
    }

    public void setOTsec(String OTsec) {
        this.OTsec = OTsec;
    }

    public String getOTHours() {
        return OTHours;
    }

    public void setOTHours(String OTHours) {
        this.OTHours = OTHours;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsHolidy() {
        return isHolidy;
    }

    public void setIsHolidy(String isHolidy) {
        this.isHolidy = isHolidy;
    }

    public String getIsLate() {
        return isLate;
    }

    public void setIsLate(String isLate) {
        this.isLate = isLate;
    }

    public String getIsEarly() {
        return isEarly;
    }

    public void setIsEarly(String isEarly) {
        this.isEarly = isEarly;
    }

    public String getIsOTEligible() {
        return isOTEligible;
    }

    public void setIsOTEligible(String isOTEligible) {
        this.isOTEligible = isOTEligible;
    }

    public String getEmpShiftId() {
        return empShiftId;
    }

    public void setEmpShiftId(String empShiftId) {
        this.empShiftId = empShiftId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public String getShiftText() {
        return shiftText;
    }

    public void setShiftText(String shiftText) {
        this.shiftText = shiftText;
    }

    public String getWeeklyOffs() {
        return weeklyOffs;
    }

    public void setWeeklyOffs(String weeklyOffs) {
        this.weeklyOffs = weeklyOffs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getPresentDays() {
        return presentDays;
    }

    public void setPresentDays(String presentDays) {
        this.presentDays = presentDays;
    }

    public String getPaidDays() {
        return paidDays;
    }

    public void setPaidDays(String paidDays) {
        this.paidDays = paidDays;
    }

    public String getAbsentDays() {
        return absentDays;
    }

    public void setAbsentDays(String absentDays) {
        this.absentDays = absentDays;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getCompLogo() {
        return compLogo;
    }

    public void setCompLogo(String compLogo) {
        this.compLogo = compLogo;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getLateDays() {
        return lateDays;
    }

    public void setLateDays(String lateDays) {
        this.lateDays = lateDays;
    }

    public String getLateHalfDays() {
        return lateHalfDays;
    }

    public void setLateHalfDays(String lateHalfDays) {
        this.lateHalfDays = lateHalfDays;
    }

    public String getLate1Day() {
        return late1Day;
    }

    public void setLate1Day(String late1Day) {
        this.late1Day = late1Day;
    }

    public String getLate2Day() {
        return late2Day;
    }

    public void setLate2Day(String late2Day) {
        this.late2Day = late2Day;
    }

    public String getLatePenalty() {
        return latePenalty;
    }

    public void setLatePenalty(String latePenalty) {
        this.latePenalty = latePenalty;
    }

    public String getTotalDurationSec() {
        return totalDurationSec;
    }

    public void setTotalDurationSec(String totalDurationSec) {
        this.totalDurationSec = totalDurationSec;
    }

    public String getTotalLateSec() {
        return totalLateSec;
    }

    public void setTotalLateSec(String totalLateSec) {
        this.totalLateSec = totalLateSec;
    }
}
