package com.cs.timeattendance.Activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.cs.timeattendance.Constants;
import com.cs.timeattendance.Model.DeviceDetails;
import com.cs.timeattendance.Adapter.DeviceTrackingAdapter;
import com.cs.timeattendance.JSONParser;
import com.cs.timeattendance.NetworkUtil;
import com.cs.timeattendance.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 05-10-2016.
 */
public class MechineTrackingActivity extends AppCompatActivity {

    ListView deviceListView;
    DeviceTrackingAdapter mAdapter;
    ArrayList<DeviceDetails> deviceList = new ArrayList<>();
    String language = "En";
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_tracking_screen);
        deviceListView = (ListView) findViewById(R.id.listView);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAdapter = new DeviceTrackingAdapter(this, deviceList, language);

        deviceListView.setAdapter(mAdapter);

        new GetAddressDetails().execute(Constants.MACHINE_TRACKING_URL);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MechineTrackingActivity.this);
            dialog = ProgressDialog.show(MechineTrackingActivity.this, "",
                    "Loading details...");
            deviceList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MechineTrackingActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(MechineTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja= new JSONArray(result);


                                for (int i = 0; i<ja.length(); i++) {

                                    DeviceDetails oh = new DeviceDetails();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String sn = jo1.getString("SN");
                                    String MachineName = jo1.getString("MachineName");
                                    String IPAddress = jo1.getString("IPAddress");
                                    String LastActivity = jo1.getString("LastActivity");



                                    oh.setSerialNo(sn);
                                    oh.setDeviceName(MachineName);
                                    oh.setIPAddress(IPAddress);
                                    oh.setLastActivity(LastActivity);

                                    deviceList.add(oh);

                                }
                            }catch (JSONException je){

                            }


                    }
                }

            }else {
                Toast.makeText(MechineTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
