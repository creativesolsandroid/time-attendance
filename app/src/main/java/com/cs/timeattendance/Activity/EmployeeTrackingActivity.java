package com.cs.timeattendance.Activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.cs.timeattendance.Adapter.DropDownAdapter;
import com.cs.timeattendance.Adapter.EmployeeAdapter;
import com.cs.timeattendance.Constants;
import com.cs.timeattendance.JSONParser;
import com.cs.timeattendance.Model.Employee;
import com.cs.timeattendance.Model.Item;
import com.cs.timeattendance.NetworkUtil;
import com.cs.timeattendance.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by CS on 4/20/2017.
 */

public class EmployeeTrackingActivity extends AppCompatActivity{

    Toolbar toolbar;

    ArrayList<Employee> employeelist = new ArrayList<>();
    ArrayList<Item> Itemlist = new ArrayList<>();


    LinearLayout memployee, mdropdown;

    ListView employeelistview, mDropDownListView;
    EmployeeAdapter mAdapter;

    String language;
    EditText medittext;

    DropDownAdapter mDropDownListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employee_tracking);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        employeelistview = (ListView) findViewById(R.id.employee_list);
        mDropDownListView = (ListView) findViewById(R.id.drop_down_list);

        memployee = (LinearLayout) findViewById(R.id.employee_layout);
        mdropdown = (LinearLayout) findViewById(R.id.drop_down_layout);

        medittext = (EditText) findViewById(R.id.edittext);

        mAdapter = new EmployeeAdapter(EmployeeTrackingActivity.this,employeelist,language);
        employeelistview.setAdapter(mAdapter);

        mdropdown.setVisibility(View.GONE);

        medittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdropdown.setVisibility(View.VISIBLE);
            }
        });

        String text = medittext.getText().toString().toLowerCase();
        mDropDownListView.invalidate();
        Itemlist.clear();
        if (text.length() == 0) {
            medittext.setGravity(Gravity.CENTER);
        }else {
            medittext.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

            for (Employee si: employeelist) {
                System.out.println("else for loop");
                if (si.getName().contains(text)) {
                    Itemlist.add((Item) si);

                    Log.i("TAG","list "+Itemlist);
                }
            }
        }

        mDropDownListAdapter = new DropDownAdapter(EmployeeTrackingActivity.this, Itemlist);
        mDropDownListView.setAdapter(mDropDownListAdapter);

        new GetEmployeedetails().execute(Constants.EMPLOYEE_TRACKING_URL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetEmployeedetails extends AsyncTask<String, Integer, String>{

        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EmployeeTrackingActivity.this);
            dialog = ProgressDialog.show(EmployeeTrackingActivity.this, "",
                    "Loading details...");
            employeelist.clear();
        }
        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EmployeeTrackingActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(EmployeeTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja= new JSONArray(result);


                            for (int i = 0; i<ja.length(); i++) {

                                Employee oh = new Employee();
                                JSONObject jo1 = ja.getJSONObject(i);
                                String userid = jo1.getString("UserId");
                                String deptid = jo1.getString("DeptId");
                                String empno = jo1.getString("EmpNo");
                                String name = jo1.getString("Name");



                                oh.setUserid(userid);
                                oh.setDeptid(deptid);
                                oh.setEmpno(empno);
                                oh.setName(name);

                                employeelist.add(oh);

                            }
                        }catch (JSONException je){

                        }


                    }
                }

            }else {
                Toast.makeText(EmployeeTrackingActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
