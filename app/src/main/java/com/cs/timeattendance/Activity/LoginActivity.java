package com.cs.timeattendance.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cs.timeattendance.Constants;
import com.cs.timeattendance.JSONParser;
import com.cs.timeattendance.MainActivity;
import com.cs.timeattendance.NetworkUtil;
import com.cs.timeattendance.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 07-10-2016.
 */
public class LoginActivity extends AppCompatActivity {

    EditText userId, password;
    RelativeLayout loginBtn;
    LinearLayout signUpBtn;
    String response;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        userId = (EditText) findViewById(R.id.user_id);
        password = (EditText) findViewById(R.id.password);
        loginBtn = (RelativeLayout) findViewById(R.id.login_btn);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userid = userId.getText().toString();
                String pwd = password.getText().toString();
                if (userid.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                        userId.setError("Please enter userID");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mEmail.setError("من فضلك أدخل رقم الجوال");
//                    }
                    userId.requestFocus();
                } else if (password.length() == 0) {
//                    if (language.equalsIgnoreCase("En")) {
                        password.setError("Please enter Password");
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        mPassword.setError("من فضلك ادخل كلمة السر");
//                    }

                    password.requestFocus();
                } else {
                    new CheckLoginDetails().execute(Constants.LOGIN_URL + userid + "&Password=" + pwd);
                }
            }
        });
    }


    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoginActivity.this);
            dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoginActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONObject jo1 = jo.getJSONObject("Success");
                                userPrefEditor.putString("login_status","loggedin");
                                userPrefEditor.commit();
                                        Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(loginIntent);
                                        finish();

                            }catch (JSONException je){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog));

                                    // set title
                                    alertDialogBuilder.setTitle("Time & Attendance");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid UserID / Password")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(LoginActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
