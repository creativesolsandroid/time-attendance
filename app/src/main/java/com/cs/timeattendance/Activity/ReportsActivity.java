package com.cs.timeattendance.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cs.timeattendance.R;

import java.util.Calendar;

/**
 * Created by CS on 08-10-2016.
 */
public class ReportsActivity extends AppCompatActivity {
    EditText startDate, endDate, empId;
    Button submitBtn;
    private int mYear, mMonth, mDay, mHour, mMinute;
    ImageView startDateBtn, endDateBtn;
    RadioButton singleShift, dualShift;
    TextView title;

    String shiftType, startDateString, endDateString, reportType;

    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_screen);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reportType = getIntent().getExtras().getString("report_type");

        title= (TextView) findViewById(R.id.header_title);

        startDate = (EditText) findViewById(R.id.start_date);
        endDate = (EditText) findViewById(R.id.end_date);
        submitBtn = (Button) findViewById(R.id.submit_btn);
        empId = (EditText) findViewById(R.id.emp_id);
        singleShift = (RadioButton) findViewById(R.id.single_shift);
        dualShift = (RadioButton) findViewById(R.id.dual_shift);
        startDateBtn = (ImageView) findViewById(R.id.start_date_icon);
        endDateBtn = (ImageView) findViewById(R.id.end_date_icon);
        startDate.setInputType(InputType.TYPE_NULL);
        endDate.setInputType(InputType.TYPE_NULL);

        if(reportType.equalsIgnoreCase("daily")){
            endDate.setVisibility(View.GONE);
            endDateBtn.setVisibility(View.GONE);
            title.setText("Daily Reports");
        }else {
            title.setText("Monthly Reports");
        }

        singleShift.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    shiftType = "SingleShift";
//                    endDate.setVisibility(View.GONE);
                }
            }
        });

        dualShift.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    shiftType = "DualShift";
//                    endDate.setVisibility(View.VISIBLE);
                }
            }
        });


        startDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                startDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                startDateString = year+", "+ (monthOfYear + 1) +", "+dayOfMonth;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        endDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ReportsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                endDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                endDateString = year+", "+ (monthOfYear + 1) +", "+dayOfMonth;
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emp_id = empId.getText().toString();
                String stDate = startDate.getText().toString();
                String edDate = startDate.getText().toString();

                if(reportType.equalsIgnoreCase("daily")){
                    endDateString = startDateString;
                    edDate = stDate;
                }

                if(emp_id.length()==0){
                    empId.setError("Please enter Employee ID");
                }else if(shiftType == null){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ReportsActivity.this, android.R.style.Theme_Material_Light_Dialog));

                        // set title
                        alertDialogBuilder.setTitle("Time & Attendance");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Please select shift type")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else if(stDate.length() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ReportsActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Time & Attendance");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Please select From date")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else if(edDate.length() == 0){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ReportsActivity.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Time & Attendance");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Please select To date")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    Intent i = new Intent(ReportsActivity.this, ReportDetailsActivity.class);
                    i.putExtra("emp_id", emp_id);
                    i.putExtra("shift_type", shiftType);
                    i.putExtra("start_date", startDateString);
                    i.putExtra("end_date", endDateString);
                    startActivity(i);
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
