package com.cs.timeattendance.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.timeattendance.Constants;
import com.cs.timeattendance.NetworkUtil;
import com.cs.timeattendance.R;
import com.cs.timeattendance.Model.Reports;
import com.cs.timeattendance.Adapter.ReportsAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by CS on 13-10-2016.
 */
public class ReportDetailsActivity extends AppCompatActivity {

    ArrayList<Reports> reportList = new ArrayList<>();
    ListView reportListview;
    TextView firstShiftTv, secondShiftTv;
    LinearLayout shiftLayout, singleShiftLayout, dualShiftLayout;
    ReportsAdapter mAdapter;
    TextView empName;

    String empId, shiftType, startDate, endDate;

    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        empId = getIntent().getExtras().getString("emp_id");
        shiftType = getIntent().getExtras().getString("shift_type");
        startDate = getIntent().getExtras().getString("start_date");
        endDate = getIntent().getExtras().getString("end_date");



        reportListview = (ListView) findViewById(R.id.reports_listview);
        empName = (TextView) findViewById(R.id.emp_name);
        shiftLayout = (LinearLayout) findViewById(R.id.shift_layout);
        singleShiftLayout = (LinearLayout) findViewById(R.id.single_shift_layout);
        dualShiftLayout = (LinearLayout) findViewById(R.id.dual_shift_layout);
        firstShiftTv = (TextView) findViewById(R.id.first_shift_tv);
        secondShiftTv = (TextView) findViewById(R.id.second_shift_tv);
        mAdapter = new ReportsAdapter(ReportDetailsActivity.this, reportList, "En", 1);

        singleShiftLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singleShiftLayout.setBackgroundColor(Color.parseColor("#800000FF"));
                firstShiftTv.setTextColor(Color.parseColor("#FFFFFF"));
                dualShiftLayout.setBackgroundColor(Color.parseColor("#EFEFEF"));
                secondShiftTv.setTextColor(Color.parseColor("#000000"));
                mAdapter = new ReportsAdapter(ReportDetailsActivity.this, reportList, "En", 1);
                reportListview.setAdapter(mAdapter);
            }
        });

        secondShiftTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singleShiftLayout.setBackgroundColor(Color.parseColor("#EFEFEF"));
                firstShiftTv.setTextColor(Color.parseColor("#000000"));
                dualShiftLayout.setBackgroundColor(Color.parseColor("#800000FF"));
                secondShiftTv.setTextColor(Color.parseColor("#FFFFFF"));
                mAdapter = new ReportsAdapter(ReportDetailsActivity.this, reportList, "En", 2);
                reportListview.setAdapter(mAdapter);
            }
        });

        if(shiftType.equalsIgnoreCase("SingleShift")){
            shiftLayout.setVisibility(View.GONE);
        }

        reportListview.setAdapter(mAdapter);

        new GetReportDetails().execute("{\"Reports\" :{\"EmpIDDetails\" : \""+empId +"\",\"ShiftType\" : \""+shiftType+"\",\"StartDate\" : \""+startDate+"\",\"EndDate\"    : \""+endDate+"\",}}");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class GetReportDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ReportDetailsActivity.this);
            dialog = ProgressDialog.show(ReportDetailsActivity.this, "",
                    "Please wait...");


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.REPORTS_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAG", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ReportDetailsActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ReportDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for(int i=0; i<ja.length(); i++){
                                    Reports reports = new Reports();
                                    JSONObject jo1= ja.getJSONObject(i);
                                    reports.setUserId(jo1.getString("UserID"));
                                    reports.setEmpNo(jo1.getString("EmpNo"));
                                    reports.setCheckIn(jo1.getString("CheckIn"));
                                    reports.setCheckOut(jo1.getString("CheckOut"));
                                    reports.setClockIn(jo1.getString("ClockIn"));
                                    reports.setClockOut(jo1.getString("ClockOut"));
                                    reports.setNClockIn(jo1.getString("NClockIn"));
                                    reports.setNClockOut(jo1.getString("NClockOut"));
                                    reports.setCInObj(jo1.getString("CInObjection"));
                                    reports.setCOutObj(jo1.getString("COutObjection"));
                                    reports.setNCInObj(jo1.getString("NCInObjection"));
                                    reports.setNCOutObj(jo1.getString("NCOutObjection"));
                                    reports.setDate(jo1.getString("Date"));
                                    reports.setDay(jo1.getString("Day"));
                                    reports.setDurationSec(jo1.getString("DurationSeconds"));
                                    reports.setLateSec(jo1.getString("LateSeconds"));
                                    reports.setEarlySec(jo1.getString("EarlySeconds"));
                                    reports.setOTsec(jo1.getString("OTSeconds"));
                                    reports.setOTHours(jo1.getString("OTHours"));
                                    reports.setRemarks(jo1.getString("Remarks"));
                                    reports.setIsHolidy(jo1.getString("IsHoliday"));
                                    reports.setIsLate(jo1.getString("IsLate"));
                                    reports.setIsEarly(jo1.getString("IsEarly"));
                                    reports.setIsOTEligible(jo1.getString("IsOTEligible"));
                                    reports.setEmpShiftId(jo1.getString("EmpShiftId"));
                                    reports.setShiftId(jo1.getString("ShiftId"));
                                    reports.setShiftName(jo1.getString("ShiftName"));
                                    reports.setShiftText(jo1.getString("ShiftText"));
                                    reports.setWeeklyOffs(jo1.getString("WeeklyOffs"));
                                    reports.setName(jo1.getString("Name"));
                                    reports.setWorkingDays(jo1.getString("WorkingDays"));
                                    reports.setPresentDays(jo1.getString("PresentDays"));
                                    reports.setPaidDays(jo1.getString("PaidDays"));
                                    reports.setAbsentDays(jo1.getString("AbsentDays"));
                                    reports.setTotalDays(jo1.getString("TotalDays"));
                                    reports.setCompLogo(jo1.getString("CompLogo"));
                                    reports.setDepartment(jo1.getString("Department"));
                                    reports.setDesignation(jo1.getString("Designation"));
                                    reports.setPhoto(jo1.getString("Photo"));
                                    reports.setLateDays(jo1.getString("LateDays"));
                                    reports.setLateHalfDays(jo1.getString("LateHalfDay"));
                                    reports.setLate1Day(jo1.getString("Late1Day"));
                                    reports.setLate2Day(jo1.getString("Late2Day"));
                                    reports.setLatePenalty(jo1.getString("LatePenalty"));
                                    reports.setTotalDurationSec(jo1.getString("TotalDurationSeconds"));
                                    reports.setTotalLateSec(jo1.getString("TotalLateSeconds"));

                                    reportList.add(reports);
                                }

                            }catch (JSONException je){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ReportDetailsActivity.this, android.R.style.Theme_Material_Light_Dialog));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ReportDetailsActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            if(reportList.size()>0){
                empName.setText(reportList.get(0).getName()+" ("+reportList.get(0).getEmpNo()+")");
            }else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ReportDetailsActivity.this, android.R.style.Theme_Material_Light_Dialog));

                // set title
                alertDialogBuilder.setTitle("Time & Attendance");

                // set dialog message
                alertDialogBuilder
                        .setMessage("No records found")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                onBackPressed();
                            }
                        });


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
