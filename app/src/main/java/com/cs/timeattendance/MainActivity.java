package com.cs.timeattendance;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.cs.timeattendance.Activity.EmployeeTrackingActivity;
import com.cs.timeattendance.Activity.LeaveInboxActivity;
import com.cs.timeattendance.Activity.LoginActivity;
import com.cs.timeattendance.Activity.MechineTrackingActivity;
import com.cs.timeattendance.Activity.ReportsActivity;

public class MainActivity extends AppCompatActivity {

    LinearLayout machineTracking, monthlyReports, dailyReports, employeeTracking, leaveInbox, objectionInbox;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        machineTracking = (LinearLayout) findViewById(R.id.machine_tracking);
        monthlyReports = (LinearLayout) findViewById(R.id.monthly_reports);
        dailyReports = (LinearLayout) findViewById(R.id.daily_reports);
        leaveInbox = (LinearLayout) findViewById(R.id.leave_inbox);
        objectionInbox = (LinearLayout) findViewById(R.id.objection_inbox);
        employeeTracking = (LinearLayout) findViewById(R.id.employee_tracking);
        machineTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MechineTrackingActivity.class);
                startActivity(i);
            }
        });

        monthlyReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ReportsActivity.class);
                i.putExtra("report_type", "monthly");
                startActivity(i);
            }
        });

        dailyReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ReportsActivity.class);
                i.putExtra("report_type", "daily");
                startActivity(i);
            }
        });

        employeeTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, EmployeeTrackingActivity.class);
                startActivity(i);

//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                // set title
//                alertDialogBuilder.setTitle("Employee Tracking");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("Coming Soon...")
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
            }
        });


        leaveInbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, LeaveInboxActivity.class);
                startActivity(i);

//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
//                // set title
//                alertDialogBuilder.setTitle("Leave Inbox");
//
//                // set dialog message
//                alertDialogBuilder
//                        .setMessage("Coming Soon...")
//                        .setCancelable(false)
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                        });
//
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
            }
        });

        objectionInbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, android.R.style.Theme_Material_Light_Dialog));

                // set title
                alertDialogBuilder.setTitle("Objection Inbox");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Coming Soon...")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });


                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            userPrefEditor.clear();
            userPrefEditor.commit();
            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
